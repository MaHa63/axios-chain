import { LOGIN_SYSTEM, FETCH_PROFILE, PROFILE_NOT_FOUND, STATUS, CLEAR_USER_RECORD } from '../actions/types';

const initialState = {
	item: {},
	account: {}
}

export default function (state = initialState, action) {
	switch(action.type) {
		case LOGIN_SYSTEM:
			console.log('Reducer:LOGIN_SYSTEM', action.payload);
				return Object.assign({}, state, {
					account: action.payload											
				})
		case FETCH_PROFILE:
			console.log('Reducer:FETCH_PROFILE', action.payload);
				return Object.assign({}, state, {
					item: action.payload
				})
		case PROFILE_NOT_FOUND:
			console.log("Reducer, PROFILE_NOT_FOUND:", action);
			return Object.assign({}, state, {
				status: action.payload,
				item: {}
			})
		case STATUS:
			console.log('Reducer,STATUS:', action.payload );
			return Object.assign({}, state, {
				status: action.payload
			})
		case CLEAR_USER_RECORD:
			console.log('Reducer,CLEAR_USER_RECORD');
			return Object.assign({}, state, {
				item: {},
				account: {}
			})
		default:
			return state;
	}
}