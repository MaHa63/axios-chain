import React, { Component } from 'react';
import './App.css';

// Redux 
import { Provider } from 'react-redux';
import store from './store';

import Login from './components/Login.js';

class App extends Component {
  render() {
    return (
			<Provider store={store}>
				<div className="App">
					<Login />
				</div>
			</Provider>
    );
  }
}

export default App;
