import React ,{Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginToSystem, fetchProfile, loginAndFetch } from '../actions/loginActions';

import { Container, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import store from '../store';

class Login extends Component {
	state = {
		name: '',
		password: ''
	}
	

	componentWillMount (){
		console.log("Login:componentWillMount:", this.props.accountInfo);
	}

	componentWillUnmount() {
		console.log("Login:componentWillUnmount:", this.props.accountInfo);
	}

	componentDidMount() {
		console.log("Login:componentDidMount:", this.props.accountInfo);
	}
	
	makeLogin = () => {
		
		const user = {
			username: this.state.name,
			password: this.state.password
		}
		console.log('Login::user:',user.username);
		console.log('Login::password:',user.password);
//		this.props.loginToSystem(user);
//		this.props.fetchProfile(2);

		this.props.loginAndFetch(user);
		this.setState({name: '' });
		this.setState({password: '' });
		console.log("Login:makeLogin:", this.props.accountInfo);
	}
	
	render(){
//		console.log("Login:render:", this.props);
//		console.log("Login:render,firstname:", this.props.login.firstname);
//		console.log("Login:render,username:", this.props.account.username);
//		console.log("Login:render,accountid:", this.props.login.accountid);

		return(
			<Container className="Login">
				<h1>Login page</h1>
				<Form className="form">
					<Col>
						<FormGroup>
							<Label>Username:</Label>
							<Input type="text" value={this.state.name} onChange={(event)=>this.setState({name: event.target.value})} />
						</FormGroup>	
					</Col>
					<Col>
						<FormGroup>
							<Label for="password">Password:</Label>
							<Input type="password" value={this.state.password} onChange={(event)=>this.setState({password: event.target.value})} />
						</FormGroup>
					</Col>
					<Button onClick={this.makeLogin}>Login</Button>
				</Form>
				<div>
				{this.props.account.username}-{this.props.login.firstname}-{this.props.login.lastname}-{this.props.account.accountid}
				</div>
			</Container>
		)
	}
}

Login.propTypes = {
	loginToSystem: PropTypes.func.isRequired,
	fetchProfile: PropTypes.func.isRequired,
	loginAndFetch: PropTypes.func.isRequired,
	login: PropTypes.object.isRequired,
	account: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
	login: state.logins.item,
	account: state.logins.account
})

export default connect(mapStateToProps, {loginToSystem, fetchProfile, loginAndFetch })(Login);