import { LOGIN_SYSTEM, FETCH_PROFILE, PROFILE_NOT_FOUND, STATUS, CLEAR_USER_RECORD } from  './types';
import axios from '../axios';

export const loginToSystem = accountData => {
	return dispatch => 
		axios.post('/login', accountData)
			.then( response => {
				console.log("Action::loginToSystem axios then:", response);
				const receivedContent = {
					username: response.data.username,
					accountid: response.data.accountid
				}

				dispatch({
					type: LOGIN_SYSTEM,
					payload: receivedContent 
				});
		
				dispatch({
					type: STATUS,
					payload: response.status 
				});
				
			})
			.catch(function(error){
				console.error("Failure during loggin phase", error.response.status);
		
				dispatch({
					type: CLEAR_USER_RECORD
				})	
		
				dispatch({
						type: STATUS,
						payload: error.response.status
				})
		
			})
	
}

export const fetchProfile = id => {
	return dispatch => 
		axios.get('/profilesofaccount/'+ id)
			.then( response => {
				console.log("Action::fetchProfile axios then:", response);
				
				if(response.status === 200) {
					dispatch({
						type: FETCH_PROFILE,
						payload: response.data[0]
					});
				} else if (response.status === 204) {
					dispatch({
						type: PROFILE_NOT_FOUND,
						payload: response.status
					});
				}
			})
			.catch(function(error) {
				console.error("Failure during fetching phase", error.response.status);
				dispatch({
						type: STATUS,
						payload: error.response.status
				})
	  })
}

export const loginAndFetch= (accountData) => {
	return (dispatch, getState)  => {
		dispatch(loginToSystem(accountData))
			.then(() => {
			  const status = getState().logins.status;
				console.log("loginAndFetch, status:", status);
				if( status === 200 ) {
					const param = getState().logins.account.accountid;
					dispatch(fetchProfile(param));
				} else {
					console.log("Failure in logging");
				}
		});
	}
}

